#!/bin/sh
source env.sh
curl --user "${KIE_SERVER_USER}:${KIE_SERVER_PASS}" -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' -d '{"lookup": "ks1", "commands": [{"insert": {"entry-point": "DEFAULT","object": {"rh.ExpenseReport": {"mcc": 1234,"amount": 4000,"justification": "Required Expense"}}}}, {"fire-all-rules": ""}, {"query": {"out-identifier": "validation_result", "name": "validations"}}]}' http://$KIE_SERVER_URL/kie-server/services/rest/server/containers/instances/rq-kjar
