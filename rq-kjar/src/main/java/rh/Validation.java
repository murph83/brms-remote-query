package rh;

/**
 * This class was automatically generated by the data modeler tool.
 */

@org.kie.api.remote.Remotable
public class Validation implements java.io.Serializable
{

   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Label(value = "Approved")
   private boolean approved;
   @org.kie.api.definition.type.Label(value = "Reason")
   private java.lang.String reason;

   public Validation()
   {
   }

   public boolean isApproved()
   {
      return this.approved;
   }

   public void setApproved(boolean approved)
   {
      this.approved = approved;
   }

   public java.lang.String getReason()
   {
      return this.reason;
   }

   public void setReason(java.lang.String reason)
   {
      this.reason = reason;
   }

   public Validation(boolean approved, java.lang.String reason)
   {
      this.approved = approved;
      this.reason = reason;
   }

}