This repository contains a kjar project `rq-kjar` as well as build/deploy/test scripts for kie-server

If using a default local installation of kie-server, no customization should be required. Otherwise, you will need to change the kie-server url and credentials found in env.sh

Usage:
```
. ./build.sh
. ./deploy.sh
. ./test.sh
```