#!/bin/sh
source env.sh
curl -X DELETE --user "${KIE_SERVER_USER}:${KIE_SERVER_PASS}" http://$KIE_SERVER_URL/kie-server/services/rest/server/containers/rq-kjar
curl -X PUT --user "${KIE_SERVER_USER}:${KIE_SERVER_PASS}" -H 'Accept: application/xml' -H 'Content-Type: application/xml' -d '<kie-container container-id="rq-kjar"><release-id><artifact-id>rq-kjar</artifact-id><group-id>rh.demo</group-id><version>1.2</version></release-id></kie-container>' http://$KIE_SERVER_URL/kie-server/services/rest/server/containers/rq-kjar
